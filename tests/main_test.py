import unittest
import json

from app import app

class Dolar(unittest.TestCase):

    def setUp(self):
        self.app = app.test_client()

    def test_indicators(self):

        # When
        response = self.app.get('/', headers={"Content-Type": "application/json"})

        # Then
        self.assertEqual(int, type(response.json['status']))
        self.assertEqual(200, response.json['status'])
        self.assertEqual(dict, type(response.json['data']))
        self.assertEqual(200, response.status_code)

    def test_history(self):

        # When
        response = self.app.get('/history/dolar', headers={"Content-Type": "application/json"})

        # Then
        self.assertEqual(int, type(response.json['status']))
        self.assertEqual(200, response.json['status'])
        self.assertEqual(dict, type(response.json['data']))
        self.assertEqual(200, response.status_code)

    def test_bad_history(self):

        # When
        response = self.app.get('/history/bad_value', headers={"Content-Type": "application/json"})

        # Then
        self.assertEqual(int, type(response.json['status']))
        self.assertEqual(400, response.json['status'])
        self.assertEqual(dict, type(response.json['data']))
        self.assertEqual(str, type(response.json['message']))
        self.assertEqual('Bad Request', response.json['message'])
        self.assertEqual(400, response.status_code)
