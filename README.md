# Bice Test Api

Prueba de desarrollo de aplicación en React utilizando Flask Api

## Comenzando 🚀

Para ver el proyecto funcionando debemos clonar el repositorio en el equipo y seguir las instrucciónes de **Instalación**


### Pre-requisitos 📋

Debes tener instalado [Docker](https://www.docker.com/)

### Instalación 🔧

Una vez clonado el repositorio, debes abrir una terminal en la ruta de la aplicación y ejecutar el siguiente comando:

```
docker build -t bice-lab-api .
```

Con este comando, crearemos la imagen de la aplicación para que pueda ejecutarse sin problemas, posterior a esto, podemos iniciar la aplicación con:

```
docker run -p 5000:5000 bice-lab-api
```

Con esto, nuestra aplicación quedará levantada en la url http://localhost:5000 o la ip utilizada por Docker

PD: Puedes dejar funcionando la aplicación en background agregando **-d** al comando **docker run**

## Construido con 🛠️

Para el desarrollo se utilizó

* [FlaskAPI](https://www.flaskapi.org/) - Implementación de Flask especializado en API

## Tests

Para Correr manualmente los tests se debe utilizar pytest

* [Pytest](https://docs.pytest.org/en/stable/) - Framework para tests

Adicionalmente se corren cada vez que se envia un nuevo commit al repositorio mediante [Gitlab CI](https://gitlab.com/Juanko/bice-lab-api/-/pipelines)