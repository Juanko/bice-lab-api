from flask_api import FlaskAPI

app = FlaskAPI(__name__)

from app import routes

if __name__ == "__main__":
    app.run( debug=True)
