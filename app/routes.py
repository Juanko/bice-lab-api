from app import app
import requests
import json

baseUrl = 'https://www.indecon.online/'

@app.route("/", methods=['GET'])
def indicators():
    """
    Get all indicators.
    """
    data = requests.get('{url}/last'.format(url=baseUrl))
    return {
        'status': 200,
        'data': data.json()
    }

@app.route("/history/<value>", methods=['GET'])
def history(value):
    """
    Get variations in history.
    """
    data = requests.get('{url}/values/{value}'.format(url=baseUrl, value=value))
    jsonData = data.json()
    if 'values' not in jsonData:
        return bad_request()
    return {
        'status': 200,
        'data': {
            'name': jsonData['name'],
            'values': jsonData['values']
        }
    }
    
@app.errorhandler(404)
def page_not_found():
    return {
        'status': 404,
        'data': {},
        'message': 'Register not found'
    }, 404

@app.errorhandler(400)
def bad_request():
    return {
        'status': 400,
        'data': {},
        'message': 'Bad Request'
    }, 400