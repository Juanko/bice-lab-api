FROM python:3.7.7-alpine

WORKDIR /usr/src/app

ADD . .

RUN pip install -r requirements.txt

EXPOSE 5000

ENV FLASK_APP=main.py

CMD ["flask","run","--host=0.0.0.0"]